```bash
git clone https://github.com/emqx/emqx-rel

cd emqx-rel
git checkout release-3.3 
# release-4.0 not stable

gedit rebar.config

```

edit the file with following:
```
{deps,
+   [ {emqx_plugin_base, {git, "https://gitlab.com/sahil8study/emqx-plugin-base", {branch, "feature_args_conf"}}}
    , emqx
    , emqx_retainer
    , emqx_management
	...
...

{relx,
    [ {include_src,false}
    ....
    , emqx
+   , ekaf
    , {mnesia, load}
    ....
+   , {emqx_plugin_base, load}
    ]}

...
```
if there is any *_build* or *_checkouts*, delete them
```bash
make
_build/emqx/rel/emqx/bin/emqx start
```
it will start the emqx broker,
go to dashboard mostly at `18083` port.
see the plugin `emqx_plugin_base` change the config if required,
as
```
## The Kafka loadbalancer node host that bridge is listening on.
## Value: 127.0.0.1, localhost
sink.host = localhost

## The kafka loadbalancer node port that bridge is listening on.
## Value: Port
sink.port = 9092

## The kafka loadbalancer node partition strategy.
## Value: random, sticky_round_robin, strict_round_robin, custom
sink.partitionstrategy = random

## Each worker represents a connection to a broker + topic + partition combination.
## You can decide how many workers to start for each partition.
## Value: 
sink.partitionworkers = 2

## payload topic.
## Value: string
sink.payloadtopic = Payload

## event topic.
## Value: string
sink.eventtopic = Event
```
start the plugin
and good to go
