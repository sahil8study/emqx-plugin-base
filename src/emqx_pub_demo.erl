-module(emqx_pub_demo).

-include_lib("emqx/include/emqx.hrl").

-export([
        init/1,
        on_message_publish/2,
        description/0
        ]).

init(Opts) -> {ok, Opts}.

on_message_publish(Message, _Env) ->
    io:fwrite("Publish ~s~n", [emqx_message:format(Message)]),
    {ok, Message}.

description() -> "Publish module".