%%--------------------------------------------------------------------
%% Copyright (c) 2019 EMQ Technologies Co., Ltd. All Rights Reserved.
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%--------------------------------------------------------------------

-module(emqx_plugin_base).

-include_lib("emqx/include/emqx.hrl").

-export([ load/1
        , unload/0
        ]).

%% Hooks functions
-export([ 
        %   on_client_authenticate/2
        % , on_client_check_acl/5
        % , on_client_connected/4
        % , on_client_disconnected/3
        % , on_client_subscribe/4
        % , on_client_unsubscribe/4
        % , on_session_created/3
        % , on_session_resumed/3
        % , on_session_terminated/3
        % , on_session_subscribed/4
        % , on_session_unsubscribed/4
          on_message_publish/2
        % , on_message_deliver/3
        % , on_message_acked/3
        % , on_message_dropped/3
        ]).

%% Called when the plugin application start
load(Env) ->
    ekaf_init(Env),
    % emqx:hook('client.authenticate', fun ?MODULE:on_client_authenticate/2, [Env]),
    % emqx:hook('client.check_acl', fun ?MODULE:on_client_check_acl/5, [Env]),
    % emqx:hook('client.connected', fun ?MODULE:on_client_connected/4, [Env]),
    % emqx:hook('client.disconnected', fun ?MODULE:on_client_disconnected/3, [Env]),
    % emqx:hook('client.subscribe', fun ?MODULE:on_client_subscribe/4, [Env]),
    % emqx:hook('client.unsubscribe', fun ?MODULE:on_client_unsubscribe/4, [Env]),
    % emqx:hook('session.created', fun ?MODULE:on_session_created/3, [Env]),
    % emqx:hook('session.resumed', fun ?MODULE:on_session_resumed/3, [Env]),
    % emqx:hook('session.subscribed', fun ?MODULE:on_session_subscribed/4, [Env]),
    % emqx:hook('session.unsubscribed', fun ?MODULE:on_session_unsubscribed/4, [Env]),
    % emqx:hook('session.terminated', fun ?MODULE:on_session_terminated/3, [Env]),
    emqx:hook('message.publish', fun ?MODULE:on_message_publish/2, [Env]).
    % emqx:hook('message.deliver', fun ?MODULE:on_message_deliver/3, [Env]),
    % emqx:hook('message.acked', fun ?MODULE:on_message_acked/3, [Env]),
    % emqx:hook('message.dropped', fun ?MODULE:on_message_dropped/3, [Env]).

% on_client_authenticate(ClientInfo = #{clientid := ClientId, password := Password}, _Env) ->
%     io:format("Client(~s) authenticate, Password:~p ~n", [ClientId, Password]),
%     {stop, ClientInfo#{auth_result => success}}.

% on_client_check_acl(#{clientid := ClientId}, PubSub, Topic, DefaultACLResult, _Env) ->
%     io:format("Client(~s) authenticate, PubSub:~p, Topic:~p, DefaultACLResult:~p~n",
%               [ClientId, PubSub, Topic, DefaultACLResult]),
%     {stop, allow}.

% on_client_connected(#{clientid := ClientId}, ConnAck, ConnAttrs, _Env) ->
%     io:format("Client(~s) connected, connack: ~w, conn_attrs:~p~n", [ClientId, ConnAck, ConnAttrs]).

% on_client_disconnected(#{clientid := ClientId}, ReasonCode, _Env) ->
%     io:format("Client(~s) disconnected, reason_code: ~w~n", [ClientId, ReasonCode]).

% on_client_subscribe(#{clientid := ClientId}, _Properties, RawTopicFilters, _Env) ->
%     io:format("Client(~s) will subscribe: ~p~n", [ClientId, RawTopicFilters]),
%     {ok, RawTopicFilters}.

% on_client_unsubscribe(#{clientid := ClientId}, _Properties, RawTopicFilters, _Env) ->
%     io:format("Client(~s) unsubscribe ~p~n", [ClientId, RawTopicFilters]),
%     {ok, RawTopicFilters}.

% on_session_created(#{clientid := ClientId}, SessAttrs, _Env) ->
%     io:format("Session(~s) created: ~p~n", [ClientId, SessAttrs]).

% on_session_resumed(#{clientid := ClientId}, SessAttrs, _Env) ->
%     io:format("Session(~s) resumed: ~p~n", [ClientId, SessAttrs]).

% on_session_subscribed(#{clientid := ClientId}, Topic, SubOpts, _Env) ->
%     io:format("Session(~s) subscribe ~s with subopts: ~p~n", [ClientId, Topic, SubOpts]).

% on_session_unsubscribed(#{clientid := ClientId}, Topic, Opts, _Env) ->
%     io:format("Session(~s) unsubscribe ~s with opts: ~p~n", [ClientId, Topic, Opts]).

% on_session_terminated(#{clientid := ClientId}, ReasonCode, _Env) ->
%     io:format("Session(~s) terminated: ~p.", [ClientId, ReasonCode]).

%% Transform message and return
on_message_publish(Message = #message{topic = <<"$SYS/", _/binary>>}, _Env) ->
    {ok, Message};

on_message_publish(Message, _Env) ->
    {ok, Payload} = format_payload(Message),
    {ok, Topic} = format_topic(Message),
    io:fwrite("Publish topic ~s~n Payload ~s~n", [Topic, Payload]),
    {ok, KafkaPayload} = format_message(Message),
    produce_kafka_payload(KafkaPayload),
    {ok, Message}.


format_message(Message) ->
    % {ok, Payload} = format_payload(Message),
    % {ok, Topic} = format_topic(Message),
    Payload = [{action, <<"message_publish">>},
                  {topic, Message#message.topic},
                  {payload, Message#message.payload}],
    {ok, Payload}.

format_topic(Message) ->
    Topic = Message#message.topic,
    {ok, Topic}.

format_payload(Message) ->
    Payload = Message#message.payload,
    {ok, Payload}.

ekaf_init(_Env) ->
    {ok, BrokerValues} = application:get_env(emqx_plugin_base, broker),
    KafkaHost = proplists:get_value(host, BrokerValues),
    KafkaPort = proplists:get_value(port, BrokerValues),
    KafkaPartitionStrategy= proplists:get_value(partitionstrategy, BrokerValues),
    KafkaPartitionWorkers= proplists:get_value(partitionworkers, BrokerValues),
    KafkaPayloadTopic = proplists:get_value(payloadtopic, BrokerValues),
    KafkaEventTopic = proplists:get_value(eventtopic, BrokerValues),
    io:fwrite("Host: ~s~nPort: ~s~nPartitionStrategy: ~s~nPartitionWorkers: ~w~nPayloadTopic: ~s~nEventTopic: ~s~n", [KafkaHost, KafkaPort, KafkaPartitionStrategy, KafkaPartitionWorkers, KafkaPayloadTopic, KafkaEventTopic]),
    application:set_env(ekaf, ekaf_bootstrap_broker,  {KafkaHost, list_to_integer(KafkaPort)}),
    application:set_env(ekaf, ekaf_partition_strategy, KafkaPartitionStrategy),
    application:set_env(ekaf, ekaf_per_partition_workers, KafkaPartitionWorkers),
    application:set_env(ekaf, ekaf_per_partition_workers_max, 10),
    io:fwrite("======================> set_env success"),
    ets:new(topic_table, [named_table, protected, set, {keypos, 1}]),
    ets:insert(topic_table, {kafka_payload_topic, KafkaPayloadTopic}),
    ets:insert(topic_table, {kafka_event_topic, KafkaEventTopic}),
    {ok, _} = application:ensure_all_started(gproc),
    io:fwrite("======================> gproc start success"),
    {ok, _} = application:ensure_all_started(ekaf),
    io:fwrite("======================> kafka-client start success").


produce_kafka_payload(Message) ->
    [{_, Topic}] = ets:lookup(topic_table, kafka_payload_topic),
    % Topic = <<"Processing">>,
	% io:format("send to kafka event topic: byte size: ~p~n", [byte_size(list_to_binary(Topic))]),    
    % Payload = iolist_to_binary(mochijson2:encode(Message)),
    Payload = jsx:encode(Message),
    % ok = ekaf:produce_async(Topic, Payload),
    ok = ekaf:produce_async(list_to_binary(Topic), Payload),
    ok.

% produce_kafka_log(Message) ->
%     [{_, Topic}] = ets:lookup(topic_table, kafka_event_topic),
%     % Topic = <<"DeviceLog">>,
%     % io:format("send to kafka event topic: byte size: ~p~n", [byte_size(list_to_binary(Topic))]),    
%     % Payload = iolist_to_binary(mochijson2:encode(Message)),
%     Payload = jsx:encode(Message),
%     % ok = ekaf:produce_async(Topic, Payload),
%     ok = ekaf:produce_async(list_to_binary(Topic), Payload),
%     ok.

% format_payload(Message) ->
%     {ClientId, Username} = format_from(Message#message.from),
%     Payload = [{action, <<"message_publish">>},
%                   {clientid, ClientId},
%                   {username, Username},
%                   {topic, Message#message.topic},
%                   {payload, Message#message.payload},
%                   {ts, emqttd_time:now_secs(Message#message.timestamp)}],
%     {ok, Payload}.

% format_from({ClientId, Username}) ->
%     {ClientId, Username};
% format_from(From) when is_atom(From) ->
%     {a2b(From), a2b(From)};
% format_from(_) ->
%     {<<>>, <<>>}.

% a2b(A) -> erlang:atom_to_binary(A, utf8).

% on_message_deliver(#{clientid := ClientId}, Message, _Env) ->
%     io:format("Deliver message to client(~s): ~s~n", [ClientId, emqx_message:format(Message)]),
%     {ok, Message}.

% on_message_acked(#{clientid := ClientId}, Message, _Env) ->
%     io:format("Session(~s) acked message: ~s~n", [ClientId, emqx_message:format(Message)]),
%     {ok, Message}.

% on_message_dropped(_By, #message{topic = <<"$SYS/", _/binary>>}, _Env) ->
%     ok;

% on_message_dropped(#{node := Node}, Message, _Env) ->
%     io:format("Message dropped by node ~s: ~s~n", [Node, emqx_message:format(Message)]);

% on_message_dropped(#{clientid := ClientId}, Message, _Env) ->
%     io:format("Message dropped by client ~s: ~s~n", [ClientId, emqx_message:format(Message)]).

%% Called when the plugin application stop
unload() ->
    % emqx:unhook('client.authenticate', fun ?MODULE:on_client_authenticate/2),
    % emqx:unhook('client.check_acl', fun ?MODULE:on_client_check_acl/5),
    % emqx:unhook('client.connected', fun ?MODULE:on_client_connected/4),
    % emqx:unhook('client.disconnected', fun ?MODULE:on_client_disconnected/3),
    % emqx:unhook('client.subscribe', fun ?MODULE:on_client_subscribe/4),
    % emqx:unhook('client.unsubscribe', fun ?MODULE:on_client_unsubscribe/4),
    % emqx:unhook('session.created', fun ?MODULE:on_session_created/3),
    % emqx:unhook('session.resumed', fun ?MODULE:on_session_resumed/3),
    % emqx:unhook('session.subscribed', fun ?MODULE:on_session_subscribed/4),
    % emqx:unhook('session.unsubscribed', fun ?MODULE:on_session_unsubscribed/4),
    % emqx:unhook('session.terminated', fun ?MODULE:on_session_terminated/3),
    emqx:unhook('message.publish', fun ?MODULE:on_message_publish/2).
    % emqx:unhook('message.deliver', fun ?MODULE:on_message_deliver/3),
    % emqx:unhook('message.acked', fun ?MODULE:on_message_acked/3),
    % emqx:unhook('message.dropped', fun ?MODULE:on_message_dropped/3).

