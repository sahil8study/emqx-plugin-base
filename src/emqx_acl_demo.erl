-module(emqx_acl_demo).

-include_lib("emqx/include/emqx.hrl").

%% ACL Callbacks
-export([ init/1
        , check_acl/5
        , reload_acl/1
        , description/0
        ]).

init(Opts) ->
    {ok, Opts}.

check_acl(#{clientid := ClientId}, PubSub, Topic, _DefaultACLResult, _Env) ->
    io:fwrite("ACL Demo: creds:~p, pubsub: ~p, topic:~p~n", [ClientId, PubSub, Topic]),
    allow.

reload_acl(_State) ->
    ok.

description() -> "ACL module".
